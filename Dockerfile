FROM maven:3.8.5-openjdk-17 as build
ENV HOME=/usr/app
RUN mkdir -p $HOME
WORKDIR $HOME
ADD . $HOME
RUN mvn package

FROM openjdk:17-oracle
COPY --from=build /usr/app/target/credit.jar /app/credit.jar
ENTRYPOINT ["java", "-jar", "/app/credit.jar"]