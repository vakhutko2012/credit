package ru.edu.credit.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "payment")
public class Payment {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "date")
    private Date date;
    @Column(name = "total_amount")
    private Long totalAmount;
    @Column(name = "body_amount")
    private Long bodyAmount;
    @Column(name = "percent_amount")
    private Long percentAmount;
    @Column(name = "co_id")
    private Long creditOfferId;
}
