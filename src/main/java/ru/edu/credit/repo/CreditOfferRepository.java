package ru.edu.credit.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.edu.credit.entity.CreditOffer;

public interface CreditOfferRepository extends JpaRepository<CreditOffer, Long> {
}
