package ru.edu.credit.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.edu.credit.entity.Credit;

public interface CreditRepository extends JpaRepository<Credit, Long> {
}
