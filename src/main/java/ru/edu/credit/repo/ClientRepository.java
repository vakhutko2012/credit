package ru.edu.credit.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.edu.credit.entity.Client;

public interface ClientRepository extends JpaRepository<Client, Long> {
}
