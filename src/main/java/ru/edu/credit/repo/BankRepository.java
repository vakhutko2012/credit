package ru.edu.credit.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.edu.credit.entity.Bank;

public interface BankRepository extends JpaRepository<Bank, Long> {
}
