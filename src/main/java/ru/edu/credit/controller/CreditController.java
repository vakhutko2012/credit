package ru.edu.credit.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import ru.edu.credit.entity.Credit;
import ru.edu.credit.repo.*;

@RestController
public class CreditController {

    private CreditRepository creditRepository;
    private ClientRepository clientRepository;
    private BankRepository bankRepository;
    private CreditOfferRepository creditOfferRepository;
    private PaymentRepository paymentRepository;

    @GetMapping(value = "/credit")
    public ModelAndView credit() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("credits", creditRepository.findAll());
        modelAndView.setViewName("credit.html");
        return modelAndView;
    }

    @GetMapping(value = "/client")
    public ModelAndView client() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("clients", clientRepository.findAll());
        modelAndView.setViewName("client.html");
        return modelAndView;
    }

    @GetMapping(value = "/bank")
    public ModelAndView bank() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("banks", bankRepository.findAll());
        modelAndView.setViewName("bank.html");
        return modelAndView;
    }

    @GetMapping(value = "/credit_offer")
    public ModelAndView creditOffer() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("creditOffers", creditOfferRepository.findAll());
        modelAndView.setViewName("credit_offer.html");
        return modelAndView;
    }

    @GetMapping(value = "/payment")
    public ModelAndView payment() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("payments", paymentRepository.findAll());
        modelAndView.setViewName("payment.html");
        return modelAndView;
    }

    @GetMapping(value = "/calc")
    public ModelAndView calcGet(Credit credit) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("calc.html");
        return modelAndView;
    }

    @PostMapping(value = "/calc")
    public String calcPost(@RequestParam String limit,
                           @RequestParam String percent) {
        System.out.println(limit);
        System.out.println(percent);
        return "Hello!";
    }

    @Autowired
    public void setCreditRepository(CreditRepository creditRepository) {
        this.creditRepository = creditRepository;
    }

    @Autowired
    public void setClientRepository(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    @Autowired
    public void setBankRepository(BankRepository bankRepository) {
        this.bankRepository = bankRepository;
    }

    @Autowired
    public void setCreditOfferRepository(CreditOfferRepository creditOfferRepository) {
        this.creditOfferRepository = creditOfferRepository;
    }

    @Autowired
    public void setPaymentRepository(PaymentRepository paymentRepository) {
        this.paymentRepository = paymentRepository;
    }
}
